<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../City.php");

class CityTest extends \PHPUnit\Framework\TestCase
{
   public function testgetCityNameById()
   {
       $city = new City();
       $result = $city->getCityNameById(1);
       $expected = 'Bordeaux';
       $this->assertTrue($result == $expected);
   }
}
